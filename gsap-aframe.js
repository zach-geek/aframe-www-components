var _gsScope = (typeof(module) !== "undefined" && module.exports && typeof(global) !== "undefined") ? global : this || window; //helps ensure compatibility with AMD/RequireJS and CommonJS/Node
  (_gsScope._gsQueue || (_gsScope._gsQueue = [])).push( function() {
     "use strict";

_gsScope._gsDefine.plugin({
    propName: 'aframe',
    priority: 0,
    API: 2,
    global: true,
    version: "1.0.0",
    init: function(target, values, tween) {
      this.target = target
      this.phonyTarget = {};

      for (const property in values)
      {
        this.phonyTarget[property] = target.data[property];

        let value = values[property];

        if (typeof value == 'function') {
           value = value();
        }

        this._addTween(this.phonyTarget, property, this.phonyTarget[property], value, property, false);
      }

      return true;
    },

    set: function(ratio) {
      this._super.setRatio.call(this, ratio);

      const oldData = AFRAME.utils.extendDeep({}, this.target.data);
      for (let prop in this.phonyTarget)
      {
        this.target.data[prop] = this.phonyTarget[prop];
      }

      this.target.update(oldData)
    }
  });

}); if (_gsScope._gsDefine) { _gsScope._gsQueue.pop()(); }
