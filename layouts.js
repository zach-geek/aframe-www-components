AFRAME.registerComponent('a-align', {
  schema: {
    x: {type: 'string'},
    y: {type: 'string'},
    z: {type: 'string'},
    xOffset: {type: 'number', default: 0},
    yOffset: {type: 'number', default: 0},
    zOffset: {type: 'number', default: 0},
    height: {type: 'number', default: undefined},
    width: {type: 'number', default: undefined},
    depth: {type: 'number', default: undefined},
    debug: {type: 'boolean'}
  },
  update(oldData) {
    const mesh = this.el.getObject3D('mesh');

    if (mesh == undefined) {
      return;
    }

    if (mesh.geometry.boundingBox == undefined) {
      mesh.geometry.computeBoundingBox();
    }

    const box = mesh.geometry.boundingBox
    const position = this.el.getAttribute('position').clone()

    for (let direction of ['x', 'y', 'z'])
    {
      const offset = this.data[direction + "Offset"];

      if (this.data[direction] === 'front' ||
          this.data[direction] === 'left' ||
          this.data[direction] === 'top')
      {
          position[direction] = -box.min[direction] - mesh.position[direction] + offset
      }
      else if (this.data[direction] === 'right' ||
          this.data[direction] === 'bottom' ||
          this.data[direction] === 'back')
      {
          position[direction] = -box.max[direction] - mesh.position[direction] + offset
      }
      else if (this.data[direction] === 'center' ||
          this.data[direction] === 'middle')
      {
          position[direction] = - mesh.position[direction] + offset
      }
    }

    this.el.setAttribute('position', position)
  }
});

AFRAME.registerComponent('a-layout', {
  schema: {
    spacing: {type: 'number', default: 0.01},
    direction: {type: 'string', default: 'x'},
    reverse: {type: "boolean"},
    align: {type: 'string'},
    rotate: {type: 'boolean'}
  },
  init() {
    this.size = new THREE.Vector3( 0.0001, 0.0001, 0.0001 )
  },
  update(oldData) {
    if (this.data.direction !== 'x' &&
        this.data.direction !== 'y' &&
        this.data.direction !== 'z' )
    {
      throw new Error("Direction is not x, y, or z")
    }

    let currentPos = 0
    let spacing = this.data.spacing
    let childList = Array.from(this.el.children);
    if (this.data.reverse)
    {
      childList = childList.reverse()
    }

    let min = this.el.object3D.position.clone();
    let max = min.clone();

    for (let el of childList)
    {
      const mesh = el.getObject3D('mesh');

      if (mesh == undefined)
      {
        continue;
      }

      if (mesh.geometry.boundingBox == undefined)
      {
        mesh.geometry.computeBoundingBox()
      }

      const box = new THREE.Box3().setFromObject(mesh)//mesh.geometry.boundingBox

      //box.applyMatrix4(el.object3D.matrix)
      //box.applyMatrix4(el.object3D.matrix)

      const position = el.getAttribute('position').clone()
      position[this.data.direction] = currentPos - box.min[this.data.direction]
      el.setAttribute('position', position)

      currentPos += box.getSize()[this.data.direction] + spacing

      let elMin = box.min.clone().add(position);
      let elMax = box.max.clone().add(position);

      min.x = Math.min(elMin.x, min.x)
      min.y = Math.min(elMin.y, min.y)
      min.z = Math.min(elMin.z, min.z)

      max.x = Math.max(elMax.x, max.x)
      max.y = Math.max(elMax.y, max.y)
      max.z = Math.max(elMax.z, max.z)


      if (this.data.debug)
      {
        //console.log(mesh, box)
        let geometry = new THREE.BoxGeometry(box.size().x, box.size().y, box.size().z);
        let material  = new THREE.MeshBasicMaterial( {color: 0x00ffff, side: THREE.DoubleSide} );
        let plane = new THREE.Mesh( geometry, material );
        plane.position.copy(position)
        material.wireframe = true;
        this.el.object3D.add(plane)
      }

      // TODO
      if (this.data.rotate)
      {
        // Set From Cylindrical
        // position.add(this.el.object3D.position)
        // let angle = this.el.object3D.position.angleTo(position)
        // console.log("Angle", angle, position, this.el.object3D.position)
      }
    }

    this.size.copy(max.sub(min));

    if (!isNaN(this.data.width))
    {
      this.size.x = this.data.width
    }

    if (!isNaN(this.data.height))
    {
      this.size.y = this.data.height
    }

    if (!isNaN(this.data.depth))
    {
      this.size.z = this.data.depth
    }

    this.size[this.data.direction] = currentPos - spacing

    const minSize = 0.001;

    this.geometry = new THREE.BoxGeometry( Math.max(this.size.x, minSize), Math.max(this.size.y, minSize), Math.max(this.size.z, minSize) );
    this.material  = new THREE.MeshBasicMaterial( {color: 0xffff00, side: THREE.DoubleSide} );
    this.plane = new THREE.Mesh( this.geometry, this.material );
    this.material.wireframe = true;
    this.plane.visible = this.data.debug ? true : false;

    this.plane.position[this.data.direction] = this.size[this.data.direction] / 2.0
    this.el.object3D.add(this.plane)
    this.el.setObject3D('mesh', this.plane)
  }
});

const layoutMappings = {
  spacing: 'a-layout.spacing',
  height: 'a-layout.height',
  width: 'a-layout.width',
  depth: 'a-layout.depth'
}

AFRAME.registerPrimitive('a-row', {
  defaultComponents: {
    "a-layout": {direction: 'x'},
  },
  mappings: layoutMappings
});

AFRAME.registerPrimitive('a-column', {
  defaultComponents: {
    "a-layout": {direction: 'y', reverse: true},
  },
  mappings: layoutMappings
});
