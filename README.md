# AFRAME WWW Components

Author: [Zach Capalbo](https://zachcapalbo.com)

## Overview

It would be cool to surf the web, check email, do banking and text processing
without having to leave virtual reality. It would be cool to do it in a native
VR way.

Here are a bunch of components to help create VR websites using
[AFRAME](https://aframe.io/) that really fit into the World Wide Web.

## Components
