AFRAME.registerPrimitive('z-well', {
  // Attaches the clock component by default
  defaultComponents: {
    "css-class": {className: "well"},
    geometry: {
      primitive: 'plane',
      height: 'auto',
      width: 5,
    },
    text: {
      font: "roboto",
      zOffset: 0.01,
      alphaTest: 0.1,
    },
    "css-color": {
      target: 'text.color'
    },
    "css-color__bg": {
      prop: 'backgroundColor',
    },
    "text-to-attr": {},
  },
  mappings: {
    value: "text.value",
    width: "geometry.width",
    height: "geometry.height"
  }
});

AFRAME.registerPrimitive('a-span', {
  // Attaches the clock component by default
  defaultComponents: {
    geometry: {
      primitive: 'plane',
      height: 'auto',
      width: 'auto',
    },
    text: {
      font: "roboto",
      zOffset: 0.01,
      alphaTest: 0.1
    },
    "css-color": {
      target: 'text.color'
    },
    "css-color__bg": {
      prop: 'backgroundColor',
    },
    "text-to-attr": {},
    material: {
      visible: false
    }
  },
  mappings: {
    value: "text.value",
    width: "geometry.width",
    height: "geometry.height"
  }
});
