// button.js was originally from https://github.com/caseyyee/aframe-ui-widgets
//
// Modifications Copyright (c) 2018 Zachary Capalbo. See LICENSE for details
//
// Original button.js license:
//
// The MIT License (MIT)
//
// Copyright (c) 2015 Kevin Ngo
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

AFRAME.registerComponent('a-ui-button', {
  schema: {
    size: { type: 'number', default: 0.1 },
    color: { type: 'color', default: '#960960' },
    pressedColor: { type: 'color', default: '#FC2907' },
    baseColor: {type: 'color', default: '#618EFF'},
    topY: { type: 'number', default: 0.02 },
    pressedY: { type: 'number', default: 0.0 },
    base: { type: 'array' },    /* specify mixin for button base */
    top: { type: 'array' },     /* specify mixin for button top */
    pressed: {type: 'array' },   /* add mixin for button when pressed */

    label: {type: 'string'},
    throttle: {default: 100 + 000},
    href: {type: 'string'}

  },

  multiple: true,

  init: function () {
    var self = this;
    this.el.classList.add('a-ui-button')
    var top = document.createElement('a-entity');
    var pseudoA = document.createElement('a');
    this.pseudoA = pseudoA
    pseudoA.classList.add("button-top")
    pseudoA.setAttribute('id', "psuedoA" + Date.now())

    for (var className of this.el.classList) {
      pseudoA.classList.add(className)
    }

    if (this.data.top.length > 0) {
      top.setAttribute('mixin', this.data.top.join(' '));
    } else {
      this.el.appendChild(pseudoA);
      // default style
      top.setAttribute('geometry', {
        primitive: 'box',
        width: 1,
        height: 0.3,
        depth: 0.02
      });
      top.setAttribute('position', { x: 0, y: this.data.topY, z: 0 });
      top.setAttribute('rotation', {x: 0, y: 0, z: 0})
      top.setAttribute('material', { color: this.data.color, transparent: true });
      top.setAttribute('text', {
        value: "Click Me",
        side: "double",
        align: "center",
        zOffset: 0.013
      })

      // Handle styles for <a> elements
      top.setAttribute('css-color', {prop: "backgroundColor", selector: "#" + pseudoA.getAttribute('id')})
      top.setAttribute('css-color__text', {prop: "color", target: "text.color", selector: "#" + pseudoA.getAttribute('id')})
    }
    this.top = top;
    this.el.appendChild(top);

    var base = document.createElement('a-entity');
    if (this.data.base.length > 0) {
      base.setAttribute('mixin', this.data.base.join(' '));
    } else {
      top.classList.add("button-base")
      // default style
      base.setAttribute('geometry', {
        primitive: 'box',
        width: 1.02,
        height: 0.302,
        depth: 0.015
      });
      base.setAttribute('material', { color: this.data.baseColor });
      base.setAttribute('css-color', {prop: "backgroundColor", selector: "#" + pseudoA.getAttribute('id')})
    }
    base.addEventListener('loaded',() => {
      this.el.setObject3D('mesh', base.getObject3D('mesh'));
    } ,{once: true})
    this.el.appendChild(base);
    this.base = base;

    var controllers = document.querySelectorAll('a-entity[hand-controls]');
    this.controllers = Array.prototype.slice.call(controllers);

    this.pressed = false;
    this.interval = null;
    this.lastTime = 0;

    this.raycaster = new THREE.Raycaster();

    this.tick = AFRAME.utils.throttleTick(this.tick, this.data.throttle, this);
  },

  play: function () {
    var el = this.el;
    // cursor controls
    el.addEventListener('mousedown', this.onButtonDown.bind(this));
    el.addEventListener('mouseup', this.onButtonUp.bind(this));
    el.addEventListener('mouseleave', this.onMouseLeave.bind(this));
    // motion controls
    el.addEventListener('hit', this.onHit);
    el.addEventListener('touchdown', this.onButtonDown.bind(this));
    el.addEventListener('touchup', this.onButtonUp.bind(this));
  },

  pause: function () {
    var el = this.el;
    el.removeEventListener('mousedown', this.onButtonDown.bind(this));
    el.removeEventListener('mouseup', this.onButtonUp.bind(this));
    el.removeEventListener('mouseleave', this.onButtonUp.bind(this));
    el.removeEventListener('hit', this.onHit);
    el.removeEventListener('touchdown', this.onButtonDown.bind(this));
    el.removeEventListener('touchup', this.onButtonUp.bind(this));
  },

  onButtonDown: function () {
    var top = this.top;
    var el = this.el;
    if (this.data.top.length > 0 && this.data.pressed.length > 0) {
      var mixin = this.data.top.join(' ') + ' ' + this.data.pressed.join(' ');
      top.setAttribute('mixin', mixin);
    } else {
      top.setAttribute('position',{ x: 0, y: this.data.pressedY, z: 0 });
      top.setAttribute('material', { color: this.data.pressedColor });

      // TODO Hack around to get the :pressed color for the pseudoA element
      this.pseudoA.classList.add('pressed')
      this.updateCss()
    }
    this.pressed = true;
    //el.emit('buttondown');
  },

  updateCss: function() {
    this.top.components['css-color'].update({})
    this.top.components['css-color__text'].update({})
    this.base.components['css-color'].update({})
  },

  resetButton: function() {
    var top = this.top;
    // top.setAttribute('position',{ x: 0, y: this.topOrigin.y, z: 0});
    if (this.data.top.length > 0) {
      var mixin = this.data.top.join(' ');
      top.setAttribute('mixin', mixin);
    } else {
      top.setAttribute('position', { x: 0, y: this.data.topY, z: 0 });
      top.setAttribute('material', { color: this.data.color });

      this.pseudoA.classList.remove('pressed')
      this.updateCss()
    }
  },

  onButtonUp: function (e) {
    if (this.pressed) {
      var el = this.el;
      this.resetButton();
      this.pressed = false;
      el.emit('buttonup');
      el.emit('pressed');

      if (this.data.href)
      {
        window.location.href = this.data.href
      }
    }
  },

  onMouseLeave: function() {
    if (this.pressed) {
      this.resetButton();
      this.pressed = false;
    }
  },

  // handles hand controller collisions
  onHit: function (evt) {
    console.log("HIT", evt)
    var threshold = 30 + this.getAttribute('a-ui-button').throttle;
    if (!this.pressed) {
      this.pressed = true;
      this.emit('touchdown');
      var self = this;
      this.interval = setInterval(function() {
        var delta = performance.now() - self.lastTime;
        if (delta > threshold) {
          self.pressed = false;
          self.lastTime = 0;
          self.emit('touchup');
          clearInterval(self.interval);
        }
      }, threshold);
    }
    this.lastTime = performance.now();
  },

  update: function () {
    this.el.setAttribute('cursor-listener','');
    this.top.setAttribute('text', 'value', this.data.label)

    // Size the button so the text fills it
    if (this.data.wrapCount)
    {
      this.top.setAttribute('text', 'wrapCount', this.data.wrapCount)
    }
    else {
      this.top.setAttribute('text', 'wrapCount', this.data.label.length)
    }
  },

  tick: function () {
    var self = this;
    var mesh = this.top.getObject3D('mesh');

    if (!mesh) {
      console.log('no mesh!');
      return
    }

    this.controllers.forEach((controller) => {
      var controllerBB;
      var collision = false;
      var collisionDetail;
      var controllerMesh = controller.getObject3D('mesh')
      if (!controllerMesh)
      {
        return
      }

      var position = controllerMesh.skeleton.bones[12].getWorldPosition()
      var matrix = new THREE.Matrix4();
      matrix.extractRotation( controllerMesh.skeleton.bones[12].matrixWorld )

      var directions = [
        [new THREE.Vector3(0, 0, -1), 0.03],
        // [new THREE.Vector3(0, -1, 0), 0.01]
      ]

      for (var direction of directions)
      {
        direction[0].applyMatrix4(matrix)
        this.raycaster.set(position, direction[0])
        this.raycaster.far=direction[1];
        collisionDetail = this.raycaster.intersectObject(this.top.object3D, true)
        collision = collisionDetail.length > 0

        if (collision) {
          break;
        }
      }

      if (collision) {
        self.el.emit('hit', collisionDetail);
      }
    });
  }
});

AFRAME.registerPrimitive('a-ui-button', {
  defaultComponents: {
    "a-ui-button": {},
    // rotation: {x: 90},
    "text-to-attr": { attr: "label"}
  },
  mappings: {
    label: "a-ui-button.label",
    href: "a-ui-button.href"
  }
})
