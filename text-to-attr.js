AFRAME.registerComponent("text-to-attr", {
  schema: {
    attr: {default: ["text", "value"], type: "array"}
  },
  multiple: true,
  init() {
    this.update()
  },
  update(oldData) {
    this.el.setAttribute(...this.data.attr, this.el.innerText || this.el.textContent)
  },
});
