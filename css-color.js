AFRAME.registerComponent("css-color", {
  schema: {
    prop: {default: "color"},
    target: {default: "material.color"},
    selector: {default: undefined},
    handleSpecialCases: {default: true}
  },
  multiple: true,
  init() {
  },
  update() {
    let el = this.el;
    if (this.data.selector !== undefined)
    {
      el = document.querySelector(this.data.selector)
    }
    css = getComputedStyle(el)
    let value = css[this.data.prop]

    if (this.data.handleSpecialCases)
    {
      value = this.transformSpecialCases(el, css, value)
    }

    if (this.data.target == "object3D")
    {
      var mesh = el.getObject3D('mesh');
      if (mesh) {
        mesh.material[this.data.prop] = css
      }
      else {
        var _this = this
        var once;
        once  = function() {
          _this.update()
          el.removeEventListener('object3dset', once)
        }
        el.addEventListener('object3dset', once)
      }
    }
    else
    {
      const target = [].concat(this.data.target.split("."), value)
      this.el.setAttribute(...target)
    }
  },
  transformSpecialCases(el, css, value) {
    if (/color/i.test(this.data.prop))
    {
      var match = /rgba\(\d+,\s*\d+,\s*\d+,\s*(\d+)\)/.exec(value)
      if (match) {
        const opacity = parseFloat(match[1]) / 255.0
        const target = [].concat(this.data.target.toString().replace(/\.color$/, ".opacity").split("."), opacity)

        this.el.setAttribute(...target)
      }
    }
    return value;
  }
});

AFRAME.registerComponent("css-class", {
  schema: {
    className: {default: ""}
  },
  multiple: true,
  init() {
  },
  update(oldData) {
    if (oldData.className != "")
    {
      this.el.classList.remove(oldData.className)
    }
    if (this.data.className != "")
    {
      this.el.classList.add(this.data.className)
    }
  }
})
