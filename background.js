AFRAME.registerComponent('background-plane', {
  schema: {
    zOffset: {default: 0.001},
    color: {type: "color", default: 0xffffff}
  },
  init() {
    this.el.addEventListener('object3dset', this.onObject3DSet.bind(this));
  },
  update(oldData) {
    const mesh = this.el.getObject3D('mesh');
    if (!mesh)
    {
      return
    }

    if (!mesh.geometry.boundingBox)
    {
      mesh.geometry.computeBoundingBox();
    }

    const size = mesh.geometry.boundingBox.getSize()

    this.geometry = new THREE.PlaneGeometry( size.x, size.y );
    this.material  = new THREE.MeshBasicMaterial( {color: this.data.color, side: THREE.DoubleSide} );
    this.plane = new THREE.Mesh( this.geometry, this.material );
    this.plane.position.x = mesh.position.x //- size.x / 2.0
    this.plane.position.y = mesh.position.y //+ size.y / 2.0
    this.plane.position.z = mesh.position.z - this.data.zOffset

    this.el.object3D.add(this.plane)
    this.el.setObject3D('background', this.plane)
  },
  onObject3DSet(name) {
    if (name === "mesh")
    {
      this.update();
    }
  }
});
