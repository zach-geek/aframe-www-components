AFRAME.registerComponent('manipulator', {
  schema: {
    selector: {type: 'string'},
    useRay: {type:'boolean', default: true},
    printUpdates: {type: 'boolean', default: false},
    lockAxes: {type: 'array', default: []} // NYI
  },
  init() {
    this.rightHand = this.el

    this.onGripClose = this.onGripClose.bind(this)
    this.onGripOpen = this.onGripOpen.bind(this)

    this.rightHand.addEventListener('gripclose', this.onGripClose)
    this.rightHand.addEventListener('gripopen', this.onGripOpen)

    this.startPoint = new THREE.Object3D()
    this.endPoint = new THREE.Object3D()

    this.startPoint.add(this.endPoint)
    this.el.sceneEl.object3D.add(this.startPoint)

    this.invM = new THREE.Matrix4()

    this.raycaster = new THREE.Raycaster();
  },
  startGrab() {
    this.startPoint.setRotationFromQuaternion(this.rightHand.object3D.getWorldQuaternion())
    this.startPoint.position.copy(this.rightHand.object3D.getWorldPosition())

    this.startPoint.updateMatrixWorld()
    this.invM.getInverse(this.startPoint.matrixWorld)

    this.endPoint.matrix.copy(this.target.object3D.matrix)
    this.endPoint.applyMatrix(this.invM)
  },
  stopGrab() {
    if (this.data.printUpdates)
    {
      console.log(this.target,
                  "\n\nposition=\"" + AFRAME.utils.coordinates.stringify(this.target.object3D.position) +"\"",
                  "\n\nrotation=\"" + AFRAME.utils.coordinates.stringify({
                    x: THREE.Math.radToDeg(this.target.object3D.rotation.x),
                    y: THREE.Math.radToDeg(this.target.object3D.rotation.y),
                    z: THREE.Math.radToDeg(this.target.object3D.rotation.z),
                  }) + "\"")
    }

    this.target = undefined
  },
  onGripClose(){
    if (this.data.useRay) {

      var bone = this.el.getObject3D('mesh').skeleton.getBoneByName('Palm')
      var position = bone.getWorldPosition()
      var matrix = new THREE.Matrix4()
      matrix.extractRotation(bone.matrixWorld)

      var directions = [
        [new THREE.Vector3(0, 0, -1), 0.3],
        [new THREE.Vector3(0, -1, 0), 0.3],
        [new THREE.Vector3(0, 1, 0), 0.3],
        // [new THREE.Vector3(0, 1, 0), 0.01]
      ]

      for (var direction of directions)
      {
        direction[0].applyMatrix4(matrix)
        this.raycaster.set(position, direction[0])
        this.raycaster.far=direction[1];

        for (var el of document.querySelectorAll(this.data.selector))
        {
          if (!el.object3D)
          {
            continue;
          }

          if (el === this.el)
          {
            continue
          }

          if (el === this.el.sceneEl)
          {
            continue
          }

          var collisionDetail = this.raycaster.intersectObject(el.object3D, true)

          if (collisionDetail.length > 0)
          {
            this.target = el
            console.log("Grabbing", el)
            this.startGrab()
            return
          }
        }
      }
    }
    else
    {
      this.target = document.querySelector(this.data.selector)
      this.startGrab()
    }
  },
  onGripOpen() {
    if (this.data.selector)
    {
      this.stopGrab()
    }
  },
  tick() {
    if (this.target) {
      this.startPoint.position.copy(this.rightHand.object3D.getWorldPosition())
      this.startPoint.setRotationFromQuaternion(this.rightHand.object3D.getWorldQuaternion())
      this.startPoint.updateMatrixWorld()

      this.target.object3D.position.copy(this.endPoint.getWorldPosition())
      this.target.object3D.setRotationFromQuaternion(this.endPoint.getWorldQuaternion())
    }
  }
})
